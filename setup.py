from setuptools import setup

setup(name='dota2gsi',
      version='0.1',
      description='DotA 2 GSI Interface',
      url='https://gitlab.com/avalonparton/dota-gsi',
      author='Avalon Parton',
      author_email='avalonlee@gmail.com',
      license='MIT',
      packages=['dota2gsi'],
      zip_safe=False)