import sys
sys.path.append('..')
sys.path.append('.')
import dota2gsi

def print_ability(state, ability):
    print("Spell cast:", ability.get('name'), 
          "level:",      ability.get('level'), 
          "cooldown:",   ability.get('cooldown'),
          "mana left:",  state.get('hero', {}).get('mana'))
          
def other_ability_handler(state, ability):
    if ability.get('ultimate'):
        time = state.get('map', {}).get('game_time')
        print("Ultimate cast at", time, "seconds")

server = dota2gsi.Server()
server.on_ability_cast(print_ability)
server.on_ability_cast(other_ability_handler)
server.start()